(() => {
  import { SpeechKitSdk } from 'https://cdn.jsdelivr.net/npm/@speechkit/speechkit-audio-player-v2@latest/dist/module/index.min.js';

  const postContent = document.getElementsByClassName('post-content');

  if (postContent.length !== 1) {
    throw Error('Unable to determine DOM location for the SpeechKit player');
  }

  const node = document.createElement('div');
  node.setAttribute('id', 'speechkit-player');
  node.style.width = '100%';
  node.style.marginBottom = '1em';

  postContent[0].insertBefore(node, postContent[0].firstChild);

  SpeechKitSdk.player({
    skBackend: 'https://staging.spkt.io',
    projectId: '3344',
    articleUrl: window.location.href,
  });
})();